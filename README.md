# Jenkins pipeline ipaclient resignation

Declarative Jenkins pipeline responsible for the following:
- Kerberos authentication
- Remove host entry from the freeipa server

### Parameters
| Parameter | Description |
| --- | --- |
| `HOSTNAME` | Name of host |
| `HOST_IP` | IPv4 |
| `DOMAIN` | Subnet domain name |
| `BOOTSTRAPP_CREDS` | SSH credentials used for bootstrapping VM's |
| `IPA` | FreeIPA credentials with host registration access |
|||

### Steps
| Step | Description |
| --- | --- |
| Declarative: Checkout SCM | Pull this repository to refresh Pipeline code |
| Kerberos Authentication | Grants access to remove host entry from the FreeIPA server |
| Delete host enry from IPA server | Remove host entry from the FreeIPA server |
|||